<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Anwar Yagoub Resume : Contact</title>

    <?php
    require_once __DIR__ . DIRECTORY_SEPARATOR . 'controls/theme_background_color_container_selector.php';
    ?>

    <!-- Include stylesheets -->
    <link rel="stylesheet" href="<?=$availableThemes[$_GET['themeName']] ?>" />

    <!-- Include font awesome stylesheets -->
    <link rel="stylesheet" href="assets/lib/font-awesome/css/font-awesome.min.css"/>

    <!-- Include custom stylesheets -->
    <link href="assets/css/style.css" type="text/css" rel="stylesheet"/>
</head>
<body style="background-color: <?=$availableBackgroundColors[$_GET['backgroundColor']] ?>">

<div class="<?=$allowedContainers[$_GET['containerType']] ?>">

    <div class="row">
        <div class="col-md-offset-6">
            <form action="" method="get" role="form" class="form-inline contents">

                <div class="form-group">
                    <select id="themeName" name="themeName" class="form-control">
                        <option value="" disabled="disabled">Select theme</option>
                        <option value="1" selected="selected">Default</option>
                        <option value="2">Cosmo</option>
                        <option value="3">Journal</option>
                        <option value="4">Lumen</option>
                    </select>
                </div>

                <div class="form-group">
                    <select id="backgroundColor" name="backgroundColor" class="form-control">
                        <option value="" disabled="disabled">Select background color</option>
                        <option value="1" selected="selected">White</option>
                        <option value="2">Moccasin</option>
                    </select>
                </div>

                <div class="form-group">
                    <select id="containerType" name="containerType" class="form-control">
                        <option value="" disabled="disabled">Select container type</option>
                        <option value="1" selected="selected">Normal</option>
                        <option value="2">Fluid</option>
                    </select>
                </div>

                <button type="submit" class="btn btn-default">Apply</button>
            </form>
        </div>
    </div>

    <div class="row links">
        <div class="col-md-offset-4 col-md-4 links">
            <div>
                <a href="index.php">Home</a>&nbsp;|&nbsp;<a href="portfolio.php">Portfolio</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-offset-4 col-md-4 col-xs-12">

            <br />

            <?php

            if (!empty($_SESSION['contactFormMsg'])) {
                ?>
                <div class="alert alert-<?= $_SESSION['contactFormMsgClass'] ?>">
                    <i class="<?= $_SESSION['contactFormMsgIcon'] ?>"></i>
                    <?= $_SESSION['contactFormMsg'] ?>
                </div>
                <?php
                unset($_SESSION['contactFormMsg'], $_SESSION['contactFormMsg'], $_SESSION['contactFormMsgIcon']);
            }
            ?>

            <form action="controls/save_contact_message.php" method="post" role="form">
            	<h3>Contact</h3>
            
            	<div class="form-group">
            		<label for="name">Name</label>
            		<input type="text" class="form-control" name="name" id="name" placeholder="Full Name">
            	</div>

                <div class="form-group">
                    <label for="email">Email Address</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="Email Address">
                </div>

                <div class="form-group">
                    <label for="message">Message:</label>
                    <textarea class="form-control" rows="5" id="message" name="message"></textarea>
                </div>
            
            	<button type="submit" class="btn btn-primary"><i class="fa fa-send"></i>&nbsp;Send</button>
            </form>

        </div>
    </div>
</div>

<!-- Include jQuery & bootstrap scripts -->
<script src="assets/lib/jquery/jquery.min.js"></script>
<script src="assets/lib/bootstrap/js/bootstrap.min.js"></script>

<!-- Include custom scripts -->
<script src="assets/js/script.js"></script>
</body>
</html>