<?php

if (!function_exists('validate_post_access')) {
    function validate_post_access() {
        return (strtolower($_SERVER['REQUEST_METHOD']) === 'post');
    }
}

if (!function_exists('validate_get_access')) {
    function validate_get_access() {
        return (strtolower($_SERVER['REQUEST_METHOD']) === 'get');
    }
}