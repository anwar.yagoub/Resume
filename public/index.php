<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Anwar Yagoub Resume</title>

    <?php
    require_once __DIR__ . DIRECTORY_SEPARATOR . 'controls/theme_background_color_container_selector.php';
    ?>

    <!-- Include stylesheets -->
    <link rel="stylesheet" href="<?=$availableThemes[$_GET['themeName']] ?>" />

    <!-- Include font awesome stylesheets -->
    <link rel="stylesheet" href="assets/lib/font-awesome/css/font-awesome.min.css" />

    <!-- Include custom stylesheets -->
    <link href="assets/css/style.css" type="text/css" rel="stylesheet" />
</head>
<body style="background-color: <?=$availableBackgroundColors[$_GET['backgroundColor']] ?>">

    <div class="<?=$allowedContainers[$_GET['containerType']] ?>">

        <div class="row">
            <div class="col-md-offset-6">
                <form action="" method="get" role="form" class="form-inline contents">

                    <div class="form-group">
                        <select id="themeName" name="themeName" class="form-control">
                            <option value="" disabled="disabled">Select theme</option>
                            <option value="1" selected="selected">Default</option>
                            <option value="2">Cosmo</option>
                            <option value="3">Journal</option>
                            <option value="4">Lumen</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <select id="backgroundColor" name="backgroundColor" class="form-control">
                            <option value="" disabled="disabled">Select background color</option>
                            <option value="1" selected="selected">White</option>
                            <option value="2">Moccasin</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <select id="containerType" name="containerType" class="form-control">
                            <option value="" disabled="disabled">Select container type</option>
                            <option value="1" selected="selected">Normal</option>
                            <option value="2">Fluid</option>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-default">Apply</button>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-md-offset-4 col-md-4 col-xs-12">
                <img class="pic" src="https://avatars1.githubusercontent.com/u/5208529?v=3&s=460" alt="Anwar Yagoub photo" />
                <h1>Anwar Yagoub</h1>
                <a href="contact.php">Contact</a>&nbsp;|&nbsp;<a href="portfolio.php">Portfolio</a>
                <h2>Linux System Engineer</h2>
                <p>Experienced Linux System Engineer with a demonstrated history of working in the information technology and services industry. Skilled in Virtualization, Linux System Administration, Configuration Management & Automation using Ansible, Amazon Web Service Cloud and Web development using PHP. Strong information technology professional with a Bachelor's degree focused in Computer Engineering from Future University.</p>
                <div class="social">
                    <a href="https://www.linkedin.com/in/anwaryagoub" target="_blank"><span class="fa fa-2x fa-linkedin-square"></span></a>
                    <a href="https://twitter.com/anwar_yagoub" target="_blank"><span class="fa fa-2x fa-twitter-square"></span></a>
                    <a href="https://github.com/AnwarYagoub" target="_blank"><span class="fa fa-2x fa-github-square"></span></a>
                </div>
            </div>
        </div>
    </div>

    <!-- Include jQuery & bootstrap scripts -->
    <script src="assets/lib/jquery/jquery.min.js"></script>
    <script src="assets/lib/bootstrap/js/bootstrap.min.js"></script>

    <!-- Include custom scripts -->
    <script src="assets/js/script.js"></script>
</body>
</html>