<?php

// available themes array
$availableThemes = [
    1 => 'assets/lib/bootstrap/css/bootstrap.min.css',
    2 => 'assets/lib/bootstrap-themes/cosmo.min.css',
    4 => 'assets/lib/bootstrap-themes/journal.min.css',
    5 => 'assets/lib/bootstrap-themes/lumen.min.css'
];

// if themeName value is not passed (no theme selected) set it to 1 (default theme)
$_GET['themeName'] = empty($_GET['themeName']) ? 1 : $_GET['themeName'];

// if not passed themeName is in available themes set it back to default
$_GET['themeName'] = array_key_exists($_GET['themeName'], $availableThemes)? $_GET['themeName'] : 1;

// available background color array
$availableBackgroundColors= [
    1 => 'white',
    2 => 'moccasin'
];

// if backgroundColor value is not passed (no background color selected) set it to 1 (default color)
$_GET['backgroundColor'] = empty($_GET['backgroundColor']) ? 1 : $_GET['backgroundColor'];

// if not passed backgroundColor is in available background colors set it back to default
$_GET['backgroundColor'] = array_key_exists($_GET['backgroundColor'], $availableBackgroundColors)? $_GET['backgroundColor'] : 1;

// allowed containers
$allowedContainers = [
    1 => 'container',
    2 => 'container-fluid'
];

// if $allowedContainers value is not passed (no container selected) set it to 1 (default color)
$_GET['containerType'] = empty($_GET['containerType']) ? 1 : $_GET['containerType'];

// if not passed $allowedContainers is in allowed containers set it back to default
$_GET['containerType'] = array_key_exists($_GET['containerType'], $allowedContainers)? $_GET['containerType'] : 1;