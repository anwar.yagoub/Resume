<?php

session_start();

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'functions' . DIRECTORY_SEPARATOR . 'request.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'functions' . DIRECTORY_SEPARATOR . 'database.php';

global $connection;

// check if request type is post
if (!validate_post_access()) die('Direct Access is not allowed');

// validate user input
if (
    !is_string($_POST['name'])
    || !is_string($_POST['email'])
    || !is_string($_POST['message'])
    || !filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL)
) {
    $_SESSION['contactFormMsg'] = "Invalid Input";
    $_SESSION['contactFormMsgClass'] = "danger";
    $_SESSION['contactFormMsgIcon'] = "fa fa-times";
    header("Location: /contact.php");
    die();
}

// escape MySQL Query Values
$_POST['name'] = mysqli_real_escape_string($connection, $_POST['name']);
$_POST['email'] = mysqli_real_escape_string($connection, $_POST['email']);
$_POST['message'] = mysqli_real_escape_string($connection, $_POST['message']);

$query="INSERT INTO `contact_message` (`name`, `email`, `message`) VALUES ('{$_POST['name']}', '{$_POST['email']}', '{$_POST['message']}')";

if (mysqli_query($connection, $query)) {
    $_SESSION['contactFormMsg'] = "saved successfully";
    $_SESSION['contactFormMsgClass'] = "success";
    $_SESSION['contactFormMsgIcon'] = "fa fa-check";
    header("Location: /contact.php");
} else {
    $_SESSION['contactFormMsg'] = "Form Error Please try again later";
    $_SESSION['contactFormMsgClass'] = "warning";
    $_SESSION['contactFormMsgIcon'] = "fa fa-exclamation";
    header("Location: /contact.php");
}