<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Anwar Yagoub Resume : Portfolio</title>

    <?php
    require_once __DIR__ . DIRECTORY_SEPARATOR . 'controls/theme_background_color_container_selector.php';
    ?>

    <!-- Include stylesheets -->
    <link rel="stylesheet" href="<?=$availableThemes[$_GET['themeName']] ?>" />

    <!-- Include font awesome stylesheets -->
    <link rel="stylesheet" href="assets/lib/font-awesome/css/font-awesome.min.css"/>

    <!-- Include custom stylesheets -->
    <link href="assets/css/style.css" type="text/css" rel="stylesheet"/>
</head>
<body style="background-color: <?=$availableBackgroundColors[$_GET['backgroundColor']] ?>">

<div class="<?=$allowedContainers[$_GET['containerType']] ?>">

    <div class="row">
        <div class="col-md-offset-6">
            <form action="" method="get" role="form" class="form-inline contents">

                <div class="form-group">
                    <select id="themeName" name="themeName" class="form-control">
                        <option value="" disabled="disabled">Select theme</option>
                        <option value="1" selected="selected">Default</option>
                        <option value="2">Cosmo</option>
                        <option value="3">Journal</option>
                        <option value="4">Lumen</option>
                    </select>
                </div>

                <div class="form-group">
                    <select id="backgroundColor" name="backgroundColor" class="form-control">
                        <option value="" disabled="disabled">Select background color</option>
                        <option value="1" selected="selected">White</option>
                        <option value="2">Moccasin</option>
                    </select>
                </div>

                <div class="form-group">
                    <select id="containerType" name="containerType" class="form-control">
                        <option value="" disabled="disabled">Select container type</option>
                        <option value="1" selected="selected">Normal</option>
                        <option value="2">Fluid</option>
                    </select>
                </div>

                <button type="submit" class="btn btn-default">Apply</button>
            </form>
        </div>
    </div>

    <div class="row links">
        <div class="col-md-offset-4 col-md-4 links">
            <div>
                <a href="index.php">Home</a>&nbsp;|&nbsp;<a href="contact.php">Contact</a>
            </div>
        </div>
    </div>

    <div class="row portfolio">

        <div class="col-md-4 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-linux">&nbsp;</i>Linux System Engineer</h3>
                </div>
                <div class="panel-body">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-server">&nbsp;</i>Network Engineer</h3>
                </div>
                <div class="panel-body">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-code">&nbsp;</i>PHP Developer</h3>
                </div>
                <div class="panel-body">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- Include jQuery & bootstrap scripts -->
<script src="assets/lib/jquery/jquery.min.js"></script>
<script src="assets/lib/bootstrap/js/bootstrap.min.js"></script>

<!-- Include custom scripts -->
<script src="assets/js/script.js"></script>
</body>
</html>