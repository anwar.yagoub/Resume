<?php
    require_once __DIR__ . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'contact_message.php';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Anwar Yagoub Resume : Display Contact Messages</title>

    <?php
    require_once __DIR__ . DIRECTORY_SEPARATOR . 'controls/theme_background_color_container_selector.php';
    ?>

<!-- Include stylesheets -->
<link rel="stylesheet" href="<?=$availableThemes[$_GET['themeName']] ?>" />

<!-- Include font awesome stylesheets -->
<link rel="stylesheet" href="assets/lib/font-awesome/css/font-awesome.min.css" />

<!-- Include custom stylesheets -->
<link href="assets/css/style.css" type="text/css" rel="stylesheet" />
</head>
<body style="background-color: <?=$availableBackgroundColors[$_GET['backgroundColor']] ?>">

<div class="<?=$allowedContainers[$_GET['containerType']] ?>">

    <div class="row">
        <div class="col-md-offset-6">
            <form action="" method="get" role="form" class="form-inline contents">

                <div class="form-group">
                    <select id="themeName" name="themeName" class="form-control">
                        <option value="" disabled="disabled">Select theme</option>
                        <option value="1" selected="selected">Default</option>
                        <option value="2">Cosmo</option>
                        <option value="3">Journal</option>
                        <option value="4">Lumen</option>
                    </select>
                </div>

                <div class="form-group">
                    <select id="backgroundColor" name="backgroundColor" class="form-control">
                        <option value="" disabled="disabled">Select background color</option>
                        <option value="1" selected="selected">White</option>
                        <option value="2">Moccasin</option>
                    </select>
                </div>

                <div class="form-group">
                    <select id="containerType" name="containerType" class="form-control">
                        <option value="" disabled="disabled">Select container type</option>
                        <option value="1" selected="selected">Normal</option>
                        <option value="2">Fluid</option>
                    </select>
                </div>

                <button type="submit" class="btn btn-default">Apply</button>
            </form>
        </div>
    </div>

    <div class="row">

        <?php
        $outputArray = get_all_contact_messages();
        if (!empty($outputArray)){
        ?>

        <table class="table table-striped table-hover contents">
        	<thead>
        		<tr>
                    <th>Send Date</th>
        			<th>Name</th>
                    <th>Email</th>
                    <th>Message</th>
        		</tr>
        	</thead>
        	<tbody>
                <?php
                    foreach ($outputArray as $row){
                        echo "<tr>";
                        echo "<td>{$row['send_date']}</td>";
                        echo "<td>{$row['name']}</td>";
                        echo "<td>{$row['email']}</td>";
                        echo "<td>{$row['message']}</td>";
                        echo "</tr>";
                    }
                ?>

        	</tbody>
        </table>

        <?php
        } else {
        ?>
            <div class="alert alert-info contents">
            	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            	<strong>Info</strong> no messages to display
            </div>
            
        <?php
        }
        ?>
    </div>
</div>

<!-- Include jQuery & bootstrap scripts -->
<script src="assets/lib/jquery/jquery.min.js"></script>
<script src="assets/lib/bootstrap/js/bootstrap.min.js"></script>

<!-- Include custom scripts -->
<script src="assets/js/script.js"></script>
</body>
</html>