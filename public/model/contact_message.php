<?php

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'functions' . DIRECTORY_SEPARATOR . 'database.php';

if (!function_exists('get_all_contact_messages')) {

    function get_all_contact_messages() {

        global $connection;

        $outputArray = [];

        $contactMessages = mysqli_query($connection, "SELECT * FROM `contact_message`;");

        while ($contactMessagesArray = mysqli_fetch_assoc($contactMessages)) {
            $outputArray[] = $contactMessagesArray;
        }

        return $outputArray;
    }

}